﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SecureArchiveTest
{
    using SecureArchive;

    [TestClass]
    public class UnitTestPath
    {
        [TestMethod]
        public void RootPathFromString ()
        {
            const string TEST_PATH = "/";
            Path path = new Path( TEST_PATH );
            Assert.AreEqual ( path.ToString (), TEST_PATH );
        }

        [TestMethod]
        public void FilePathFromString ()
        {
            const string TEST_PATH = "/file.txt";
            Path path = new Path( TEST_PATH );
            Assert.AreEqual ( path.ToString (), TEST_PATH );
        }

        [TestMethod]
        public void FolderPathFromString ()
        {
            const string TEST_PATH = "/test/path";
            Path path = new Path( TEST_PATH );
            Assert.AreEqual ( path.ToString (), TEST_PATH );
        }

        [TestMethod]
        public void FolderWithFilePathFromString ()
        {
            const string TEST_PATH = "/test/path/file.txt";
            Path path = new Path( TEST_PATH );
            Assert.AreEqual ( path.ToString (), TEST_PATH );
            Assert.AreEqual ( path.LastItem, "file.txt" );
        }

        [TestMethod]
        public void RealPath ()
        {
            const string TEST_PATH = @"C:\Program Files\WinRAR";
            Path path = new Path( TEST_PATH );
            Assert.AreEqual ( path.ToString (), TEST_PATH );
        }

        [TestMethod]
        public void RootPathFromEntry ()
        {
            Assert.AreEqual ( new Root ().Path.ToString(), "/" );
        }

        [TestMethod]
        public void FolderPathFromEntry ()
        {
            Root root = new Root();
            Folder folder = new Folder( root, "test" );
            Assert.AreEqual ( folder.Path.ToString (), "/test" );
        }

        [TestMethod]
        public void FilePathFromEntry ()
        {
            Root root = new Root();
            File file = new File( root, "file.txt", null );
            Assert.AreEqual ( file.Path.ToString (), "/file.txt" );
        }


        [TestMethod]
        public void DeepPathFromEntry ()
        {
            Root root = new Root();
            Folder folder1 = new Folder( root, "folder1" );
            Folder folder2 = new Folder( folder1, "folder2" );
            Folder folder3 = new Folder( folder2, "folder3" );
            File folder3file = new File( folder3, "file.txt", null );

            Assert.AreEqual ( folder3file.Path[ 2 ], "folder3" );
            Assert.AreEqual ( folder3file.Path [ 3 ], "file.txt" );
            Assert.AreEqual ( folder3file.Path.ToString (), "/folder1/folder2/folder3/file.txt" );
        }


        [TestMethod]
        public void SubPath ()
        {
            Path path = new Path( "/test/path/file.txt" );
            Assert.AreEqual ( path.SubPath ( 1 ).ToString (), "/path/file.txt" );
            Assert.AreEqual ( path.SubPath ( 2 ).ToString (), "/file.txt" );
        }

        [TestMethod]
        public void ParentPath ()
        {
            Path path = new Path( "/test/path/file.txt" );
            Assert.AreEqual ( path.ParentPath ( 1 ).ToString (), "/test/path" );
            Assert.AreEqual ( path.ParentPath ( 2 ).ToString (), "/test" );
        }
    }
}
