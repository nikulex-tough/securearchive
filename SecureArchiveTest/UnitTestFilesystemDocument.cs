﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace SecureArchiveTest
{
    using SecureArchive;

    [TestClass]
    public class UnitTestFilesystemDocument
    {
        private FilesystemDocument CreateDocumentAndImport ()
        {
            FilesystemDocument doc = new FilesystemDocument();
            doc.ImportToRoot ( "import" );
            return doc;
        }

        private Path CreateFile ( string realPath, string content = "TEST" )
        {
            Path path = new Path ( realPath );

            string parent = path.ParentPath().ToString ();
            if ( !Directory.Exists ( parent ) )
                Directory.CreateDirectory ( parent );
            
            System.IO.File.WriteAllText ( realPath, content );
            return path;
        }

        private Path CreateFolder ( string realPath )
        {
            Path path = new Path ( realPath );
            Directory.CreateDirectory ( realPath );
            return path;
        }

        [TestMethod]
        public void ImportFile()
        {
            const string TEST_FILE = "test.txt";
            const string TEST_CONTENT = "HELLO";

            CreateFile ( TEST_FILE, TEST_CONTENT );

            FilesystemDocument doc = new FilesystemDocument();
            doc.ImportToRoot ( TEST_FILE );

            Assert.IsNotNull ( doc.Filesystem.Root [ TEST_FILE ] );
            Assert.IsNotNull ( ( doc.Filesystem.Root [ TEST_FILE ] as File ).Data );
            Assert.AreEqual ( ( doc.Filesystem.Root [ TEST_FILE ] as File ).DataAsString, TEST_CONTENT );
        }

        [TestMethod]
        public void ImportFolder ()
        {
            Path testFolder = CreateFolder ( "import/test" );

            FilesystemDocument doc = CreateDocumentAndImport ();

            Assert.IsNotNull ( doc.Filesystem.Root [ testFolder.LastItem ] );
        }

        [TestMethod]
        public void ImportFolderAndFile ()
        {
            Path testFolder = CreateFolder ( "import/test" );
            Path testFile = CreateFile ( "import/test.txt", "HELLO" );

            FilesystemDocument doc = CreateDocumentAndImport ();
            
            Assert.IsNotNull ( doc.Filesystem.Root [ testFolder.LastItem ] );
            Assert.IsNotNull ( doc.Filesystem.Root [ testFile.LastItem ] );
        }

        [TestMethod]
        public void ImportHeirarchy ()
        {
            CreateFolder ( "import/test" );
            CreateFolder ( "import/test/test" );
            CreateFolder ( "import/test/empty" );
            CreateFolder ( "import/test2" );

            CreateFile ( "import/test.txt" );
            CreateFile ( "import/test2.txt" );
            CreateFile ( "import/test/test.txt" );
            CreateFile ( "import/test/test/other.txt" );
            CreateFile ( "import/test2/file.txt" );

            FilesystemDocument doc = CreateDocumentAndImport ();

            Assert.IsNotNull ( doc.Filesystem.Root [ "test" ] as Folder );
            Assert.IsNotNull ( doc.Filesystem.Root [ "test" ] [ "test" ] as Folder );
            Assert.IsNotNull ( doc.Filesystem.Root [ "test" ] [ "empty" ] as Folder );
            Assert.IsNotNull ( doc.Filesystem.Root [ "test2" ] as Folder );

            Assert.IsNotNull ( doc.Filesystem.Root [ "test.txt" ] as File );
            Assert.IsNotNull ( doc.Filesystem.Root [ "test2.txt" ] as File );
            Assert.IsNotNull ( doc.Filesystem.Root [ "test" ] [ "test.txt" ] as File );
            Assert.IsNotNull ( doc.Filesystem.Root [ "test" ] [ "test" ] [ "other.txt" ] as File );
            Assert.IsNotNull ( doc.Filesystem.Root [ "test2" ] [ "file.txt" ] as File );
        }

        [TestMethod]
        public void Crypt ()
        {
            string data = "Hello World";
            string encrypted = Crypter.Encrypt ( data );
            string decrypted = Crypter.Decrypt ( encrypted );
            Assert.AreEqual ( data, decrypted );
        }

        [TestMethod]
        public void SaveRestore ()
        {
            FilesystemBuilder builder = UnitTestFilesystem.CreateSimpleHierarchy ();
            FilesystemDocument doc = new FilesystemDocument( builder.Filesystem );
            doc.RealPath = new Path ( "test.xml" );
            doc.Save ();

            FilesystemDocument docRestored = FilesystemDocument.FromFile ( doc.RealPath.ToString () );

            Assert.AreEqual (
                    doc.Filesystem.Root.DumpHierarchy()
                ,   docRestored.Filesystem.Root.DumpHierarchy ()
            );
        }
    }
}
