﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SecureArchiveTest
{
    using SecureArchive;

    [TestClass]
    public class UnitTestNodes
    {
        [TestMethod]
        public void EmptyRootNode ()
        {
            Root testRoot = new Root();
            Assert.AreEqual ( testRoot.Name, "ROOT" );
            Assert.AreSame ( testRoot.Parent, null );
            Assert.AreEqual ( testRoot.Count, 0 );
        }
        
        [TestMethod]
        public void FileNode ()
        {
            const string TEST_FILE_NAME = "file.txt";
            const string TEST_FILE_CONTENT = "some data";

            Root root = new Root();
            File testFile = new File( root, TEST_FILE_NAME, TEST_FILE_CONTENT);
            Assert.AreEqual ( testFile.Name, TEST_FILE_NAME );
            Assert.AreSame ( testFile.Parent, root );
            Assert.AreEqual ( testFile.Object, TEST_FILE_CONTENT );
        }

        [TestMethod]
        public void EmptyFolderNode ()
        {
            const string TEST_FOLDER_NAME = "test";

            Root root = new Root();
            Folder testFolder = new Folder( root, TEST_FOLDER_NAME );
            Assert.AreEqual ( testFolder.Name, TEST_FOLDER_NAME );
            Assert.AreEqual ( testFolder.Parent, root );
            Assert.AreEqual ( testFolder.Count, 0 );
        }

        [TestMethod]
        public void RootFolderNodeWithFile ()
        {
            const string TEST_FILE_NAME = "test.txt";

            Root root = new Root();
            File testFile = new File( root, TEST_FILE_NAME, "DATA" );
            Assert.AreEqual ( root.Count, 1 );
            Assert.AreSame ( root[ TEST_FILE_NAME ], testFile );
        }


        [TestMethod]
        public void RootFolderNodeWithFolderWithFile ()
        {
            const string TEST_FOLDER_NAME = "test";
            const string TEST_FILE_NAME = "test.txt";

            Root root = new Root();
            Folder testFolder = new Folder( root, TEST_FOLDER_NAME );
            File testFile = new File( testFolder, TEST_FILE_NAME, "Test data" );

            Assert.AreEqual ( root.Count, 1 );
            Assert.AreEqual ( testFolder.Count, 1 );

            Assert.AreSame ( root [ TEST_FOLDER_NAME ], testFolder );
            Assert.AreSame ( root [ TEST_FOLDER_NAME ] [ TEST_FILE_NAME ], testFile );
            Assert.AreSame ( testFolder [ TEST_FILE_NAME ], testFile );
        }

        [TestMethod]
        public void RootFolderNodeWithFolderAndFile ()
        {
            const string TEST_FOLDER_NAME = "test";
            const string TEST_FILE_NAME = "file.txt";

            Root root = new Root();
            Folder testFolder = new Folder( root, TEST_FOLDER_NAME );
            File testFile = new File( root, TEST_FILE_NAME, "data" );

            Assert.AreEqual ( root.Count, 2 );
            Assert.AreEqual ( testFolder.Count, 0 );

            Assert.AreSame ( root [ TEST_FOLDER_NAME ], testFolder );
            Assert.AreSame ( root [ TEST_FOLDER_NAME ] [ TEST_FILE_NAME ], null );
            Assert.AreSame ( root [ TEST_FILE_NAME ], testFile );
        }

        [TestMethod]
        public void MoveNodeFile ()
        {
            const string TEST_FOLDER_NAME = "test";
            const string TEST_FILE_NAME = "file.txt";

            Root root = new Root();
            Folder testFolder = new Folder( root, TEST_FOLDER_NAME );
            File testFile = new File( root, TEST_FILE_NAME, "data" );

            Assert.AreSame ( root [ TEST_FILE_NAME ], testFile );
            Assert.AreSame ( root [ TEST_FOLDER_NAME ], testFolder );
            Assert.AreSame ( root [ TEST_FOLDER_NAME ] [ TEST_FILE_NAME ], null );

            testFile.Move ( testFolder );

            Assert.AreSame ( root [ TEST_FILE_NAME ], null );
            Assert.AreSame ( root [ TEST_FOLDER_NAME ], testFolder );
            Assert.AreSame ( root [ TEST_FOLDER_NAME ] [ TEST_FILE_NAME ], testFile );
        }

        [TestMethod]
        public void MoveNodeFolder ()
        {
            const string TEST_FOLDER_NAME1 = "test";
            const string TEST_FOLDER_NAME2 = "test2";
            const string TEST_FILE_NAME = "file.txt";

            Root root = new Root();
            Folder testFolder1 = new Folder( root, TEST_FOLDER_NAME1 );
            Folder testFolder2 = new Folder( root, TEST_FOLDER_NAME2 );
            File testFile = new File( testFolder2, TEST_FILE_NAME, "data" );
            
            Assert.AreSame ( root [ TEST_FOLDER_NAME1 ], testFolder1 );
            Assert.AreSame ( root [ TEST_FOLDER_NAME1 ] [ TEST_FOLDER_NAME2 ], null );
            Assert.AreSame ( root [ TEST_FOLDER_NAME2 ], testFolder2 );
            Assert.AreSame ( root [ TEST_FOLDER_NAME2 ] [ TEST_FILE_NAME ], testFile );

            testFolder2.Move ( testFolder1 );
            
            Assert.AreSame ( root [ TEST_FOLDER_NAME1 ], testFolder1 );
            Assert.AreSame ( root [ TEST_FOLDER_NAME2 ], null );
            Assert.AreSame ( root [ TEST_FOLDER_NAME1 ] [ TEST_FOLDER_NAME2 ], testFolder2 );
            Assert.AreSame ( root [ TEST_FOLDER_NAME1 ] [ TEST_FOLDER_NAME2 ] [ TEST_FILE_NAME ], testFile );
        }

        [TestMethod]
        [ExpectedException( typeof ( ArgumentException ) )]
        public void UniqueChild ()
        {
            const string TEST_FILE_NAME = "test.txt";

            Root root = new Root();
            File testFile1 = new File( root, TEST_FILE_NAME, "DATA" );
            File testFile2 = new File( root, TEST_FILE_NAME, "DATA" );
        }
    }
}
