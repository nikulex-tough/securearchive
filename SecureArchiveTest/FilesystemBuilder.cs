﻿using System;
using System.Text;
using SecureArchive;

namespace SecureArchiveTest
{
    public class FilesystemBuilder
    {
        public Filesystem Filesystem { get; }

        public FilesystemBuilder( Filesystem filesystem )
        {
            if ( filesystem == null )
                throw new ArgumentNullException ();
            Filesystem = filesystem;
        }

        public FilesystemBuilder Raise()
        {
            if ( Filesystem.WorkingFolder != Filesystem.Root )
                Filesystem.WorkingFolder = Filesystem.WorkingFolder.Parent;
            return this;
        }

        public FilesystemBuilder RaiseRoot()
        {
            Filesystem.WorkingFolder = Filesystem.Root;
            return this;
        }

        public FilesystemBuilder AddFile( string name, string data )
        {
            return AddFile ( name, Encoding.UTF8.GetBytes ( data ) );
        }

        public FilesystemBuilder AddFile ( string name, byte[] data )
        {
            new File ( Filesystem.WorkingFolder, name, data );
            return this;
        }

        public FilesystemBuilder AddFolder( string name )
        {
            Folder folder = new Folder( Filesystem.WorkingFolder, name );
            Filesystem.WorkingFolder = folder;
            return this;
        }
    }
}
