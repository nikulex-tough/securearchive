﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SecureArchiveTest
{
    using SecureArchive;

    [TestClass]
    public class UnitTestFilesystem
    {
        public static FilesystemBuilder CreateSimpleHierarchy()
        {
            FilesystemBuilder builder = new FilesystemBuilder( new Filesystem() );
            builder
                .AddFile ( "rootfile.txt", "rootfile.txt content" )
                .AddFolder ( "rootfolder" )
                    .AddFile ( "file.txt", "file.txt content" )
                    .AddFolder ( "folder" )
                        .AddFolder ( "emptyfolder" ).Raise ()
                        .AddFile ( "deepfile.txt", "deepfile.txt content" );
            return builder;
        }

        [TestMethod]
        public void InitState ()
        {
            Filesystem filesystem = new Filesystem();
            Assert.AreEqual ( filesystem.Root.Count, 0 );
            Assert.AreEqual ( filesystem.WorkingFolder, filesystem.Root );
        }

        [TestMethod]
        public void SimpleHierarchyDump()
        {
            const string EXPECTED_DUMP
                =   "[ROOT]\n"
                +   "\trootfile.txt\n"
                +   "\t[rootfolder]\n"
                +   "\t\tfile.txt\n"
                +   "\t\t[folder]\n"
                +   "\t\t\t[emptyfolder]\n"
                +   "\t\t\tdeepfile.txt";
            
            FilesystemBuilder builder = CreateSimpleHierarchy();
            string result = builder.Filesystem.Root.DumpHierarchy ();
            Assert.AreEqual ( EXPECTED_DUMP, builder.Filesystem.Root.DumpHierarchy () );
        }

        [TestMethod]
        public void SimpleHierarchyDumpAtWorkingFolder ()
        {
            const string EXPECTED_DUMP
                =   "[folder]\n"
                +   "\t[emptyfolder]\n"
                +   "\tdeepfile.txt";
            
            FilesystemBuilder builder = CreateSimpleHierarchy();
            Assert.AreEqual ( EXPECTED_DUMP, builder.Filesystem.WorkingFolder.DumpHierarchy () );
        }
    }
}
