﻿using System;

namespace SecureArchive
{
    [Serializable]
    public class Filesystem
    {
        public Root Root { get; } = new Root ();
        public Folder WorkingFolder { get; set; }
        
        public Filesystem ()
        {
            WorkingFolder = Root;
        }

        public static void Move ( Node node, Folder parent )
        {
            if ( node.Parent != null )
                node.Parent.Remove ( node );
            parent.Add ( node );
        }
    }
}
