﻿using System;
using System.IO;

namespace SecureArchive
{
    [Serializable]
    public class FilesystemDocument
    {
        public Filesystem Filesystem { get; }
        public string Password { get; set; }
        public Path RealPath { get; set; }

        public FilesystemDocument ()
        {
            Filesystem = new Filesystem ();
        }

        public FilesystemDocument( Filesystem filesystem )
        {
            Filesystem = filesystem;
        }

        #region Import

        public void ImportToRoot ( string realPath ) => Import ( Filesystem.Root, realPath );

        public void ImportToWorkingFolder ( string realPath ) => Import ( Filesystem.WorkingFolder, realPath );

        public static void Import ( Folder parent, string realPath )
        {
            if ( System.IO.File.Exists ( realPath ) )
                ImportFile ( parent, realPath );
            else if ( Directory.Exists ( realPath ) )
                ImportFolder ( parent, realPath );
            else
                throw new Exception ( "Error path" );
        }

        private static void ImportFolder ( Folder parent, string path )
        {
            foreach ( string node in Directory.GetFileSystemEntries ( path ) )
            {
                try
                {
                    if ( System.IO.File.Exists ( node ) )
                        ImportFile ( parent, node );

                    else if ( Directory.Exists ( node ) )
                    {
                        Folder folder = new Folder( parent, node.Substring ( node.LastIndexOf ( '\\' ) + 1 ) );
                        ImportFolder ( folder, node );
                    }
                }
                catch ( Exception )
                {
                }
            }
        }

        private static void ImportFile ( Folder parent, string path )
        {
            try
            {
                using ( FileStream fs = System.IO.File.OpenRead ( path ) )
                {
                    using ( MemoryStream ms = new MemoryStream () )
                    {
                        fs.CopyTo ( ms );
                        new File (
                                parent
                            ,   new Path ( path ).LastItem
                            ,   ms.GetBuffer ()
                        );
                    }
                }
            }
            catch ( Exception )
            {
            }
        }

        #endregion

        #region Export

        public void ExportAll ( string path ) => Export ( Filesystem.Root, path );
        
        public static void Export ( Node node, string path )
        {
            string nodePath = ( new Path ( path ) / node.ToString () ).ToString ();

            if ( node is File )
                ExportFile ( node as File, nodePath );

            else if ( node is Folder )
                ExportFolder ( node as Folder, nodePath );
        }

        private static void ExportFolder ( Folder parent, string path )
        {
            Directory.CreateDirectory ( path );
            foreach ( Node node in parent )
                Export ( node, path );
        }

        private static void ExportFile ( File node, string path )
        {
            using ( MemoryStream ms = new MemoryStream ( node.Data ) )
            {
                using ( FileStream fs = new FileStream ( path, FileMode.OpenOrCreate ) )
                    ms.CopyTo ( fs );
            }
        }

        #endregion

        public static FilesystemDocument FromFile ( string realPath )
        {
            return ( FilesystemDocument ) System.IO.File
                .ReadAllBytes ( realPath )
                .Decrypt ()
                .BinDeserialize ();
        }

        public void Save ()
        {
            if ( RealPath == null )
                return;

            System.IO.File.WriteAllBytes (
                    RealPath.ToString ()
                ,   this.BinSerialize ().Encrypt ()
            );
        }
    }
}