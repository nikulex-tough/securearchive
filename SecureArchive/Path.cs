﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecureArchive
{
    [Serializable]
    public class Path
    {
        private List < string > items = new List < string > ();

        public PathSeparator Separator { get; set; }
        public char SeparatorChar => Separator == PathSeparator.Slash ? '/' : '\\';

        public Path ( PathSeparator separator = PathSeparator.Slash )
        {
            Separator = separator;
        }

        public Path ( string path )
        {
            Separator = path.Contains ( "\\" )
                ?   PathSeparator.BackSlash
                :   PathSeparator.Slash;

            items.AddRange ( path.Split (
                    new char [] { '/', '\\' }
                ,   StringSplitOptions.RemoveEmptyEntries
            ) );
        }
        
        public Path ( Node node )
        {
            Separator = PathSeparator.Slash;

            Node currentNode = node;
            if ( currentNode != null )
            {
                do
                {
                    string name = currentNode.ToString();
                    if ( name?.Length > 0 )
                        items.Add ( currentNode.ToString() );
                    currentNode = currentNode.Parent;
                } while ( currentNode != null );
            }
            items.Reverse ();
        }

        public Path Add ( string item )
        {
            items.Add ( item );
            return this;
        }

        public static Path operator / ( Path p1, Path p2 )
        {
            Path result = new Path ();
            result.items.AddRange ( p1.items );
            result.items.AddRange ( p2.items );
            result.Separator
                =   ( p1.Separator == PathSeparator.BackSlash || p2.Separator == PathSeparator.BackSlash )
                ?   PathSeparator.BackSlash
                :   PathSeparator.Slash;
            return result;
        }

        public static Path operator / ( Path p, string entry )
        {
            if ( entry == string.Empty )
                return p;

            Path result = new Path ();
            result.Separator = p.Separator;
            result.items.AddRange ( p.items );
            result.items.Add ( entry );
            return result;
        }

        public Path SubPath ( int level = 1 )
        {
            Path result = new Path ();
            result.Separator = Separator;
            for ( int i = level; i < items.Count; ++i )
                result.Add ( this [ i ] );
            return result;
        }

        public Path ParentPath ( int level = 1 )
        {
            Path result = new Path ();
            result.Separator = Separator;
            for ( int i = 0; i < items.Count - level; ++i )
                result.Add ( this [ i ] );
            return result;
        }

        public string this[ int index ] => items [ index ];
        public int Count => items.Count;

        public string LastItem
            =>  ( items.Count > 0 )
            ?   items [ items.Count - 1 ]
            :   string.Empty;

        public override string ToString ()
        {
            StringBuilder builder = new StringBuilder();
            
            if ( Separator == PathSeparator.Slash )
                builder.Append ( SeparatorChar );
            
            for ( int i = 0; i < items.Count - 1; ++i )
            {
                builder.Append ( items [ i ] );
                builder.Append ( SeparatorChar );
            }

            builder.Append ( LastItem );

            return builder.ToString();
        }
    }
}
