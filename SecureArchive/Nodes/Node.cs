﻿using System;

namespace SecureArchive
{
    [Serializable]
    public abstract class Node
    {
        public Folder Parent { get; private set; }
        public string Name { get; set; }

        public Node ()
        {
            // Serialize
        }

        public Node ( Folder parent, string name )
        {
            Parent = parent;
            Name = name;
            if ( Parent != null )
                Parent.Add ( this );
        }

        public void Move ( Folder newParent )
        {
            if ( Parent != null )
                Parent.Remove ( this );

            Parent = newParent;

            if ( Parent != null )
                Parent.Add ( this );
        }

        public virtual Node this [ string name ] => null;

        public Path Path => new Path ( this );

        public override string ToString () => Name;
    }
}
