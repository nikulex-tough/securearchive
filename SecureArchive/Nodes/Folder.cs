﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SecureArchive
{
    [Serializable]
    public class Folder
        :   Node
        ,   IEnumerable<Node>
    {
        #region Colleciton

        private List<Node> children = new List<Node> ();

        public IEnumerator<Node> GetEnumerator () => children.GetEnumerator ();
        IEnumerator IEnumerable.GetEnumerator () => children.GetEnumerator ();

        public Node this [ int index ] => children [ index ];
        public int Count => children.Count;

        public void Add ( Node node )
        {
            foreach ( Node child in children )
            {
                if ( node.Name == child.Name && node.GetType () == child.GetType () )
                    throw new ArgumentException ( "Already exist" );
            }
            children.Add ( node );
        }

        public void AddRange ( ICollection<Node> nodes )
        {
            foreach ( Node node in nodes )
                Add ( node );
        }

        public void Remove ( Node node ) => children.Remove ( node );

        #endregion

        public Folder ()
        {
            // Serialize
        }

        public Folder ( Folder parent, string name )
            :   base ( parent, name )
        {
        }

        public override Node this [ string name ] => children.Find ( ( e ) => e.Name == name );

        public Node GetNodeByPath ( Path path ) => GetNodeByPath ( path, 0, this );

        private Node GetNodeByPath ( Path path, int depth, Folder parent )
        {
            if ( depth >= path.Count )
                return null;

            foreach ( Node entry in parent.children )
            {
                if ( entry.Name == path [ depth ] )
                {
                    if ( depth == path.Count - 1 )
                        return entry;
                    else if ( entry is Folder && depth < path.Count )
                        return GetNodeByPath ( path, depth + 1, entry as Folder );
                    else
                        return null;
                }
            }
            return null;
        }

        public string DumpHierarchy () => TreeDumper.Dump ( this );
    }
}
