﻿using System;
using System.IO;

namespace SecureArchive
{
    [Serializable]
    public class File : Node
    {
        public byte[] Data { get; set; }

        public string DataAsString
        {
            get
            {
                using ( StreamReader reader = new StreamReader ( new MemoryStream ( Data ) ) )
                    return reader.ReadToEnd ().TrimEnd ( '\0' );
            }
        }

        public object Object => SerializeHelper.BinDeserialize ( Data );

        public File ( Folder parent, string name, byte[] data )
            :   base ( parent, name )
        {
            Data = data;
        }

        public File ( Folder parent, string name, object data )
            :   this ( parent, name, SerializeHelper.BinSerialize ( data ) )
        {
        }
    }
}
