﻿using System;

namespace SecureArchive
{
    [Serializable]
    public class Root : Folder
    {
        public Root()
            :   base( null, "ROOT" )
        {
        }

        public override string ToString () => string.Empty;
    }
}
