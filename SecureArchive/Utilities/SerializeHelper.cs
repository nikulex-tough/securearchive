﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace SecureArchive
{
    public static class SerializeHelper
    {
        public static byte[] XmlSerialize ( this object obj )
        {
            using ( MemoryStream ms = new MemoryStream () )
            {
                new XmlSerializer ( obj.GetType () ).Serialize ( ms, obj );
                return ms.GetBuffer ();
            }
        }

        public static object XmlDeserialize ( this byte[] buffer, Type type )
        {
            using ( MemoryStream ms = new MemoryStream ( buffer ) )
                return new XmlSerializer ( type ).Deserialize ( ms );
        }

        public static byte[] BinSerialize ( this object obj )
        {
            using ( MemoryStream ms = new MemoryStream () )
            {
                new BinaryFormatter ().Serialize ( ms, obj );
                return ms.GetBuffer ();
            }
        }

        public static object BinDeserialize ( this byte[] buffer )
        {
            using ( MemoryStream ms = new MemoryStream ( buffer ) )
                return new BinaryFormatter ().Deserialize ( ms );
        }
    }
}
