﻿using System.Text;

namespace SecureArchive
{
    public class TreeDumper
    {
        private StringBuilder builder = new StringBuilder();

        private int depth;
        private string Indent
        {
            get
            {
                string intent = string.Empty;
                for ( int i = 0; i < depth; ++i )
                    intent += "\t";
                return intent;
            }
        }
        
        public static string Dump ( Folder node, int depth = 0 )
        {
            TreeDumper dumper = new TreeDumper();
            dumper.Build ( node );
            return dumper.builder.ToString ();
        }

        private void Build ( Folder node )
        {
            builder.Append ( Indent + "[" + node.Name + "]" );
            ++depth;
            foreach ( Node child in node )
            {
                if ( child is File )
                    builder.Append ( "\n" + Indent + child.Name );
                else if ( child is Folder )
                {
                    builder.Append ( "\n" );
                    Build ( child as Folder );
                }
            }
            --depth;
        }
    }
}
