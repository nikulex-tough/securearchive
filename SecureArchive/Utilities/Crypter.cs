﻿using System.Linq;
using System.Text;

namespace SecureArchive
{
    public static class Crypter
    {
        public const int key = 123;

        public static string Encrypt ( this string text )
        {
            if ( text == null )
                return null;

            byte[] bytes = Encoding.UTF8.GetBytes ( text );
            return Encoding.UTF8.GetString ( Encrypt ( bytes ) );
        }

        public static string Decrypt ( this string text )
        {
            return Encrypt ( text );
        }

        public static byte[] Encrypt ( this byte[] data )
        {
            for ( int i = 0; i < data.Length; ++i )
                data [ i ] = ( byte ) ( data [ i ] ^ key );
            data.Reverse ();
            return data;
        }

        public static byte[] Decrypt ( this byte[] data )
        {
            return Encrypt ( data );
        }
    }
}
