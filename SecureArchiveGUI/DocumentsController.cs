﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SecureArchive;

namespace SecureArchiveGUI
{
    public class DocumentsController
    {
        public const string EXTENTION = ".sarch";
        public string Filter => $"Secure Archive ({EXTENTION})|*{EXTENTION}|All Files (*.*)|*.*";

        public FilesystemDocument Document { get; private set; }
        public bool IsEdit { get; private set; }
        
        public DocumentsController ()
        {
        }

        public void Remove ( List<Node> nodes )
        {
            if ( Document == null || nodes == null || nodes.Count == 0 )
                return;

            foreach ( Node node in nodes )
                Document.Filesystem.WorkingFolder.Remove ( node );

            IsEdit = Document.Filesystem.Root.Count > 0;
        }

        public void Rename ( Node node, string newName )
        {
            if ( node.Name != newName )
            {
                IsEdit = true;
                node.Name = newName;
            }
        }

        public Folder CreateFolder ( Folder parent )
        {
            if ( Document == null )
                return null;
            IsEdit = true;
            return new Folder ( parent, Utilities.AutoRename ( parent, "New folder" ) );
        }

        public Folder CreateFolderWorkingFolder ()
        {
            return CreateFolder ( Document.Filesystem.WorkingFolder );
        }
        
        public void MoveEntries ( List<Node> nodes, Folder parent )
        {
            foreach ( Node node in nodes )
            {
                Node child = parent[ node.Name ];
                if ( child == null )
                    node.Move ( parent );
                else
                {
                    if ( MessageBox.Show (
                            "Node allready exist! Do you want rename node?"
                        ,   "Node allready exist!"
                        ,   MessageBoxButtons.YesNo
                        ) == DialogResult.Yes )
                    {
                        node.Name = Utilities.AutoRename ( parent, node.Name );
                        node.Move ( parent );
                    }
                }
            }
            if ( nodes.Count > 0 )
                IsEdit = true;
        }

        public void SetPassword ( string password )
        {
            if ( Document != null )
            {
                Document.Password = password;
                IsEdit = true;
            }
        }

        public void SetPasswordDialog ()
        {
            if ( Document == null )
                return;

            SetPasswordForm setPassword = new SetPasswordForm ( Document.Password );
            if ( setPassword.ShowDialog () == DialogResult.OK )
                SetPassword ( setPassword.Result );
        }

        #region Init Document

        public void NewDocument ()
        {
            if ( AskSaveIfEdit () )
            {
                Document = new FilesystemDocument ( new Filesystem () );
                IsEdit = false;
            }
        }

        public void OpenDocument ( string path )
        {
            if ( !AskSaveIfEdit () )
                return;

            Document = FilesystemDocument.FromFile ( path );
            
            if ( Document.Password?.Length > 0 )
            {
                EnterPasswordForm enterPassword = new EnterPasswordForm ( Document );
                if ( enterPassword.ShowDialog () != DialogResult.OK )
                    Document = null;
                else
                    IsEdit = false;
            }
        }

        public void OpenDocumentDialog ()
        {
            try
            {
                OpenFileDialog openFile = new OpenFileDialog ();
                openFile.Filter = Filter;
                openFile.FilterIndex = 0;
                if ( openFile.ShowDialog () == DialogResult.OK )
                    OpenDocument ( openFile.FileName );
            }
            catch ( Exception ex )
            {
                MessageBox.Show (
                        "Error open document!\n" + ex.Message
                    ,   "Error"
                    ,   MessageBoxButtons.OK
                    ,   MessageBoxIcon.Error
                );
            }
        }

        public void Reset ()
        {
            if ( AskSaveIfEdit () )
                Document = null;
        }

        #endregion

        #region Save

        public void Save ()
        {
            if ( Document != null )
            {
                if ( Document.RealPath == null )
                    SaveAs ();
                else
                {
                    Document.Save ();
                    IsEdit = false;
                }
            }
        }

        public void SaveAs ()
        {
            SaveFileDialog saveFile = new SaveFileDialog ();
            saveFile.Filter = Filter;
            saveFile.FilterIndex = 0;
            if ( saveFile.ShowDialog () == DialogResult.OK )
            {
                Document.RealPath = new SecureArchive.Path ( saveFile.FileName );
                Document.Save ();
                IsEdit = false;
            }
        }

        public bool AskSaveIfEdit ()
        {
            if ( Document != null && IsEdit )
            {
                switch ( MessageBox.Show (
                        "Do you want save?"
                    ,   "Save"
                    ,   MessageBoxButtons.YesNoCancel
                    ,   MessageBoxIcon.Question
                ) )
                {
                    case DialogResult.Yes:
                        Save ();
                        break;
                    case DialogResult.No:
                        IsEdit = false;
                        break;
                    case DialogResult.Cancel:
                        return false;
                }
            }
            return true;
        }

        #endregion
        
        #region Import

        public void ImportFilesDialog ()
        {
            if ( Document == null )
                return;
            try
            {
                OpenFileDialog openFiles = new OpenFileDialog ();
                openFiles.Multiselect = true;
                if ( openFiles.ShowDialog () == DialogResult.OK )
                {
                    foreach ( string realPath in openFiles.FileNames )
                        Document.ImportToWorkingFolder ( realPath );
                }
            }
            catch ( Exception )
            {
                MessageBox.Show ( "Import error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
        }

        public void ImportFolderDialog ()
        {
            if ( Document == null )
                return;
            try
            {
                FolderBrowserDialog openFolder = new FolderBrowserDialog ();
                if ( openFolder.ShowDialog () == DialogResult.OK )
                    Document.ImportToWorkingFolder ( openFolder.SelectedPath );
            }
            catch ( Exception )
            {
                MessageBox.Show ( "Import error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
        }

        #endregion

        #region Export

        private static void ExportCheck ( Folder parent, string path )
        {
            foreach ( Node child in parent )
            {
                if ( child is SecureArchive.File )
                {
                    if ( System.IO.File.Exists ( path ) )
                    {
                        if ( MessageBox.Show ( $"Do you want replace file: {path} ?", "Replace file", MessageBoxButtons.YesNoCancel )
                            == DialogResult.No )
                            return;
                    }
                }
                else
                    ExportCheck ( child as Folder, path );
            }
        }

        public void ExportDialog ()
        {
            if ( Document == null )
                return;
            
            FolderBrowserDialog export = new FolderBrowserDialog ();
            if ( export.ShowDialog () == DialogResult.OK )
            {
                ExportCheck ( Document.Filesystem.Root, export.SelectedPath );
                Document.ExportAll ( export.SelectedPath );
            }
        }

        #endregion
    }
}
