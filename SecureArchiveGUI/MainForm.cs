﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using SecureArchive;

/* v 24
    Разработать программное обеспечение для защищенного хранения группы файлов в виде единого файла-контейнера
    (как это делается в любом архиваторе, с шифрованием, но без компрессии).
    Программное обеспечение должно обеспечивать возможности по добавлению и удалению файла из контейнера,
    извлечению сохраненных данных из защищенного контейнера.
    Желательно реализовать возможность хранения иерархических файловых систем (файлы + папки).
*/


namespace SecureArchiveGUI
{

    public partial class MainForm : Form
    {
        private DocumentsController controller = new DocumentsController ();
        private Configurations config;
        private FilesystemListView listView;

        public MainForm ()
        {
            InitializeComponent ();
            
            listView = new FilesystemListView ( panelWrapper );
            LoadIcons ();
            ApplyEventHandlers ();
            LoadConfigurations ();
            UpdateView ();
        }

        private void LoadConfigurations ()
        {
            config = Configurations.Load ();
            if ( config == null )
            {
                config = new Configurations ();
                NewDocument ();
            }
            else
            {
                try
                {
                    controller.OpenDocument ( config.LastDocumentPath );
                }
                catch
                {
                    config.LastDocumentPath = null;
                    NewDocument ();
                }
            }
        }

        private void SaveConfigurations ()
        {
            try
            {
                config.Save ();
            }
            catch ( Exception ex )
            {
                MessageBox.Show ( ex.Message, "Save configurations error" );
            }
        }

        private void LoadIcons ()
        {
            Icon = Icon.FromHandle ( ( Image.FromFile ( "img/app.png" ) as Bitmap ).GetHicon () );

            newToolStripMenuItem.Image = Image.FromFile ( "img/new.png" );
            openToolStripMenuItem.Image = Image.FromFile ( "img/open.png" );
            saveToolStripMenuItem.Image = Image.FromFile ( "img/save.png" );
            saveAsToolStripMenuItem.Image = saveToolStripMenuItem.Image;

            importFilesToolStripMenuItem.Image = Image.FromFile ( "img/add_file.png" );
            importFolderToolStripMenuItem.Image = Image.FromFile ( "img/add_folder.png" );
            exportToolStripMenuItem.Image = Image.FromFile ( "img/download.png" );
            removeToolStripMenuItem.Image = Image.FromFile ( "img/delete.png" );
            createFolderToolStripMenuItem.Image = Image.FromFile ( "img/new_folder.png" );
            moveToolStripMenuItem.Image = Image.FromFile ( "img/refresh.png" );
            passwordToolStripMenuItem.Image = Image.FromFile ( "img/lock.png" );
            renameToolStripMenuItem.Image = Image.FromFile ( "img/edit.png" );
        }

        private void ApplyEventHandlers ()
        {
            // Document view
            listView.SelectedIndexChanged += ( s, e ) => UpdatePathTextBox ();
            listView.SelectedIndexChanged += ( s, e ) => UpdateMenuItemsEnabling ();
            listView.FolderChanged += UpdatePathTextBox;
            listView.FolderChanged += UpdateMenuItemsEnabling;
            listView.KeyUp += HotKeysHandler;
            listView.MouseDown += OnMouseDownDocumentView;
            listView.MouseUp += OnMouseUpDocumentView;
            listView.AfterLabelEdit += OnRenameFinished;

            // Form
            FormClosing += ( s, e ) => e.Cancel = !controller.AskSaveIfEdit ();
            FormClosed += ( s, e ) => SaveConfigurations ();

            // File menu items
            newToolStripMenuItem.Click += ( s, e ) => NewDocument ();
            openToolStripMenuItem.Click += ( s, e ) => OpenDocument ();
            saveToolStripMenuItem.Click += ( s, e ) => SaveDocument ();
            saveAsToolStripMenuItem.Click += ( s, e ) => SaveDocumentAs ();
            exitToolStripMenuItem.Click += ( s, e ) =>
            {
                if ( controller.AskSaveIfEdit () )
                    Close ();
            };

            // Actions menu items
            importFilesToolStripMenuItem.Click += ( s, e ) => ImportFiles ();
            importFolderToolStripMenuItem.Click += ( s, e ) => ImportFolder ();
            exportToolStripMenuItem.Click += ( s, e ) => ExportFiles ();
            createFolderToolStripMenuItem.Click += ( s, e ) => CreateFolder ();
            moveToolStripMenuItem.Click += ( s, e ) => MoveSelectedNodesDialog ();
            removeToolStripMenuItem.Click += ( s, e ) => RemoveSelectedItems ();
            passwordToolStripMenuItem.Click += ( s, e ) => SetPassword ();
            renameToolStripMenuItem.Click += ( s, e ) => RenameSelectedItems ();
        }

        private void HotKeysHandler ( object sender, KeyEventArgs e )
        {
            if ( e.Control )
            {
                switch ( e.KeyCode )
                {
                    case Keys.N: NewDocument (); break;
                    case Keys.O: OpenDocument (); break;
                    case Keys.I: ImportFiles (); break;
                    case Keys.E: ExportFiles (); break;
                    case Keys.P: SetPassword (); break;
                    case Keys.D: CreateFolder (); break;
                    case Keys.M: MoveSelectedNodesDialog (); break;
                    case Keys.S:
                        if ( e.Shift )
                            SaveDocumentAs ();
                        else
                            SaveDocument ();
                        break;
                }
            }

            else if ( e.Shift )
            {
                switch ( e.KeyCode )
                {
                    case Keys.F: ImportFiles (); break;
                    case Keys.D: ImportFolder (); break;
                    case Keys.E: ExportFiles (); break;
                }
            }

            else
            {
                switch ( e.KeyCode )
                {
                    case Keys.Delete: RemoveSelectedItems (); break;
                    case Keys.F2: RenameSelectedItems (); break;
                }
            }
        }
        
        private void OnMouseDownDocumentView ( object s, MouseEventArgs e )
        {
            listView.Cursor = Cursors.Hand;
        }

        private void OnMouseUpDocumentView ( object s, MouseEventArgs e )
        {
            listView.Cursor = Cursors.Arrow;
            if ( e.Button == MouseButtons.Left )
            {
                ListViewItem targetItem = listView.GetItemAt ( e.Location.X, e.Location.Y );
                if ( targetItem?.Tag != null )
                {
                    try
                    {
                        Folder targetNode = targetItem.Tag as Folder;
                        if ( targetNode != null && !listView.Nodes.Contains ( targetNode ) )
                            MoveSelectedNodes ( targetNode );
                    }
                    catch
                    {

                    }
                }
            }
        }
        
        private void OnRenameFinished ( object s, LabelEditEventArgs e )
        {
            if ( !e.CancelEdit )
            {
                ListViewItem item = listView.Items[ e.Item ];
                Node node = item?.Tag as Node;
                if ( node != null )
                {
                    string extention = Utilities.GetExtention ( node.Name );
                    item.SubItems[ 0 ].Name = e.Label;
                    controller.Rename ( node, e.Label + extention );
                    UpdateView ();
                    listView.IsRenaming = false;
                }
            }
        }

        #region Update

        private void UpdatePathTextBox ()
        {
            if ( controller.Document != null )
            {
                if ( listView.SelectedItems.Count > 0 )
                {
                    Node node = listView.SelectedItems [ 0 ].Tag as Node;
                    if ( node != null )
                        textBoxPath.Text = node.Path.ToString ();
                }
                textBoxPath.Text = controller.Document.Filesystem.WorkingFolder.Path.ToString ();
                return;
            }
            textBoxPath.Text = "Document is not selected";
        }

        private void UpdateTitleText ()
        {
            Text = "Secure Archive";
            if ( controller.Document != null )
            {
                Text += ": ";

                if ( controller.IsEdit )
                    Text += "[edit] ";

                Text
                    +=  ( controller.Document.RealPath != null )
                    ?   controller.Document.RealPath.ToString ()
                    :   "New document";
            }
        }

        private void UpdateMenuItemsEnabling ()
        {
            bool existDocument = ( controller.Document != null );
            saveToolStripMenuItem.Enabled = existDocument;
            saveAsToolStripMenuItem.Enabled = existDocument;
            actionsToolStripMenuItem.Enabled = existDocument;
            removeToolStripMenuItem.Enabled = listView.SelectedItems.Count > 0;
            moveToolStripMenuItem.Enabled = removeToolStripMenuItem.Enabled;
            renameToolStripMenuItem.Enabled = removeToolStripMenuItem.Enabled;
        }

        private void UpdateView ()
        {
            UpdateTitleText ();
            UpdatePathTextBox ();
            UpdateMenuItemsEnabling ();
            config.LastDocumentPath = controller.Document?.RealPath?.ToString ();
            listView.Filesystem = controller.Document?.Filesystem;
            listView.RefillTree ();
        }

        #endregion

        #region Actions

        private void NewDocument ()
        {
            controller.NewDocument ();
            UpdateView ();
        }

        private void OpenDocument ()
        {
            controller.OpenDocumentDialog ();
            UpdateView ();
        }

        private void ResetDocument ()
        {
            controller.Reset ();
            UpdateView ();
        }

        private void CreateFolder ()
        {
            Folder folder = controller.CreateFolderWorkingFolder ();
            UpdateView ();

            foreach ( ListViewItem item in listView.Items )
            {
                Folder findFolder = item.Tag as Folder;
                if ( findFolder == folder )
                {
                    item.BeginEdit ();
                    break;
                }
            }
        }

        private void ImportFiles ()
        {
            controller.ImportFilesDialog ();
            UpdateView ();
        }

        private void ImportFolder ()
        {
            controller.ImportFolderDialog ();
            UpdateView ();
        }

        private void ExportFiles ()
        {
            controller.ExportDialog ();
        }

        private void SaveDocument ()
        {
            controller.Save ();
            UpdateTitleText ();
        }

        private void SaveDocumentAs ()
        {
            controller.SaveAs ();
            if ( !controller.IsEdit )
                config.LastDocumentPath = controller.Document?.RealPath?.ToString ();
            UpdateTitleText ();
        }

        private void RemoveSelectedItems ()
        {
            try
            {
                controller.Remove ( listView.Nodes );
                listView.SelectedItems.Clear ();
                UpdateView ();
            }
            catch ( Exception )
            {
                MessageBox.Show (
                        "Error remove items!"
                    ,   "Error"
                    ,   MessageBoxButtons.OK
                    ,   MessageBoxIcon.Error
                );
            }
        }

        private void RenameSelectedItems ()
        {
            var selected = listView.SelectedItems;
            if ( selected.Count == 1 && selected [ 0 ].Tag != null )
            {
                selected [ 0 ].BeginEdit ();
                listView.IsRenaming = true;
            }
        }

        private void MoveSelectedNodesDialog ()
        {
            // TODO Dialog select
        }

        private void MoveSelectedNodes ( Folder parent )
        {
            controller.MoveEntries ( listView.Nodes, parent );
            UpdateView ();
        }

        private void SetPassword ()
        {
            controller.SetPasswordDialog ();
            UpdateView ();
        }

        #endregion
    }
}
