﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SecureArchiveGUI
{
    [Serializable]
    public class Configurations
    {
        public string LastDocumentPath { get; set; }

        #region File operations

        public const string CONFIG_FILE_NAME = "SecureArchiveGUI.config";

        public void Save ()
        {
            using ( FileStream fs = new FileStream ( CONFIG_FILE_NAME, FileMode.Create ) )
                new XmlSerializer ( GetType () ).Serialize ( fs, this );
        }

        public static Configurations Load ()
        {
            try
            {
                if ( File.Exists ( CONFIG_FILE_NAME ) )
                {
                    using ( FileStream fs = new FileStream ( CONFIG_FILE_NAME, FileMode.Open ) )
                        return ( Configurations ) new XmlSerializer ( typeof ( Configurations ) ).Deserialize ( fs );
                }
            }
            catch
            {
            }
            return null;
        }

        #endregion
    }
}
