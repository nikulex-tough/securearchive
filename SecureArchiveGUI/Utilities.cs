﻿using SecureArchive;

namespace SecureArchiveGUI
{
    public static class Utilities
    {
        public static string AutoRename( Folder parent, string name )
        {
            if ( parent [ name ] == null )
                return name;

            int count = 0;
            string rename;
            string[] parts = SeparateFileAndExtention ( name );
            do
            {
                ++count;
                rename = $"{ parts [ 0 ] }({ count.ToString () }) { parts [ 1 ] }";
            }
            while ( parent [ rename ] != null );
            return rename;
        }

        public static string[] SeparateFileAndExtention ( string fileName )
        {
            string name = fileName;
            string extention = string.Empty;

            int extentionIndex = name.LastIndexOf ( "." );
            if ( extentionIndex != -1 )
            {
                extention = name.Substring ( extentionIndex );
                name = name.Replace ( extention, "" );
            }
            return new string [] { name, extention };
        }

        public static string GetExtention ( string fileName )
        {
            int extentionIndex = fileName.LastIndexOf ( "." );
            if ( extentionIndex != -1 )
                return fileName.Substring ( extentionIndex );
            return string.Empty;
        }

        public static string GetWithoutExtention ( string fileName )
        {
            return SeparateFileAndExtention ( fileName ) [ 0 ];
        }
    }
}
