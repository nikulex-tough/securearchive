﻿using System;
using System.Windows.Forms;

namespace SecureArchiveGUI
{
    public partial class SetPasswordForm : Form
    {
        public SetPasswordForm ( string text = "" )
        {
            InitializeComponent ();
            textBoxPassword.Text = text;
        }

        public string Result => textBoxPassword.Text;

        private void checkBoxShow_CheckedChanged ( object sender, EventArgs e )
        {
            textBoxPassword.PasswordChar = checkBoxShow.Checked ? '\0' : '*'; 
        }

        private void buttonOK_Click ( object sender, EventArgs e )
        {
            DialogResult = DialogResult.OK;
            Close ();
        }

        private void buttonCancel_Click ( object sender, EventArgs e )
        {
            Close ();
        }
    }
}
