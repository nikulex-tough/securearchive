﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

using SecureArchive;

namespace SecureArchiveGUI
{
    public class FilesystemListView : ListView
    {
        public Filesystem Filesystem { get; set; }
        public bool IsRenaming { get; set; }

        public FilesystemListView( Control parent )
        {
            parent.Controls.Add ( this );

            Dock = DockStyle.Fill;
            HeaderStyle = ColumnHeaderStyle.Clickable;
            Name = "listView";
            UseCompatibleStateImageBehavior = false;
            AllowColumnReorder = true;
            LabelEdit = true;

            View = View.Details;
            GridLines = true;
            FullRowSelect = true;

            Columns.Add ( "File", 200 );
            Columns.Add ( "Extantion", 200 );
            Columns.Add ( "Size", 200 );
            
            ImageList images = new ImageList();
            images.Images.Add ( "folder", Image.FromFile ( "img/folder.png" ) );
            images.Images.Add ( "file", Image.FromFile ( "img/file.png" ) );
            LargeImageList = images;
            SmallImageList = images;
            

            MouseDoubleClick += ( s, e ) => SelectedEntryAction ();
            KeyUp += ( o, e ) =>
            {
                if ( !IsRenaming && ( e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return ) )
                    SelectedEntryAction ();
                else if ( e.KeyCode == Keys.Back )
                    MoveBack ();
            };
        }
        
        public event Action FolderChanged;

        public void MoveBack()
        {
            if ( Filesystem != null )
            {
                if ( Filesystem.WorkingFolder != Filesystem.Root )
                    Filesystem.WorkingFolder = Filesystem.WorkingFolder.Parent;
                FolderChanged ();
                RefillTree ();
            }
        }

        public void SelectedEntryAction ()
        {
            if ( Filesystem != null && SelectedItems.Count > 0 )
            {
                object item = SelectedItems [ 0 ].Tag;

                if ( item == null )
                {
                    MoveBack ();
                }
                else if ( item is File )
                {
                    MessageBox.Show ( ( item as File ).DataAsString, "File content" );
                }
                else if ( item is Folder )
                {
                    Filesystem.WorkingFolder = item as Folder;
                    FolderChanged ();
                    RefillTree ();
                }
            }
        }

        public List<Node> Nodes
        {
            get
            {
                List<Node> list = new List<Node> ();
                foreach ( ListViewItem item in SelectedItems )
                {
                    if ( item.Tag != null )
                        list.Add ( item.Tag as Node );
                }
                return list;
            }
        }

        public static void OnRename ( ListViewItem item, string name )
        {

        }

        public void RefillTree ()
        {
            if ( Filesystem == null )
                return;

            Items.Clear ();

            if ( Filesystem.WorkingFolder != Filesystem.Root )
            {
                ListViewItem back = new ListViewItem();
                back.Text = "...";
                back.ImageKey = "folder";
                Items.Add ( back );
            }

            foreach ( Node node in Filesystem.WorkingFolder )
            {
                if ( node is Folder )
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = node.Name;
                    item.ImageKey = "folder";
                    item.Tag = node;
                    Items.Add ( item );
                }
                else if ( node is File )
                {
                    string[] parts = Utilities.SeparateFileAndExtention ( node.Name );

                    ListViewItem item = new ListViewItem();
                    item.Text = parts[ 0 ];
                    item.ImageKey = "file";
                    item.Tag = node;
                    item.SubItems.Add ( parts[ 1 ] );
                    
                    byte[] data = ( node as File ).Data;
                    if ( data != null )
                        item.SubItems.Add ( data.Length.ToString () );
                    else
                        item.SubItems.Add ( "null" );

                    Items.Add ( item );
                }
            }
        }
    }
}
