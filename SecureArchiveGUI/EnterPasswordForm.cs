﻿using System;
using System.Drawing;
using System.Windows.Forms;

using SecureArchive;

namespace SecureArchiveGUI
{
    public partial class EnterPasswordForm : Form
    {
        private FilesystemDocument document;

        public EnterPasswordForm ( FilesystemDocument document )
        {
            InitializeComponent ();
            this.document = document;
        }

        private void buttonEnter_Click ( object sender, EventArgs e )
        {
            if ( textBoxPassword.Text == document.Password )
            {
                DialogResult = DialogResult.OK;
                Close ();
            }
            else
                textBoxPassword.BackColor = Color.FromArgb ( 255, 200, 200 );
        }
    }
}
